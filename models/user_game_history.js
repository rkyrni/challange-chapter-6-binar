"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class user_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.user_game_history.belongsTo(models.user_game, {
        foreignKey: "user_id",
      });
    }
  }
  user_game_history.init(
    {
      winrate: {
        type: DataTypes.STRING,
        defaultValue: "0%",
      },
      win: {
        type: DataTypes.STRING,
        defaultValue: "0",
      },
      lose: {
        type: DataTypes.STRING,
        defaultValue: "0",
      },
      triple_kill: {
        type: DataTypes.STRING,
        defaultValue: "0",
      },
      user_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "user_game_history",
    }
  );
  return user_game_history;
};
