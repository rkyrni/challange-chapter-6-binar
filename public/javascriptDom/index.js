const sidebarEl = document.querySelector("#sidebar");
const btnSidebarEl = document.querySelector("#btnSidebar");
const itemSidebarEl = document.querySelectorAll(".list-item-sidebar");
console.log(btnSidebarEl.classList[0]);

btnSidebarEl.addEventListener("click", () => {
  sidebarEl.classList.toggle("sidebarHidden");

  itemSidebarEl.forEach((item) => {
    item.classList.toggle("d-none");
  });

  if (btnSidebarEl.textContent === "Open") btnSidebarEl.textContent = "Close";
  else btnSidebarEl.textContent = "Open";

  const btnOpenCloseSidebar = btnSidebarEl.classList[1];
  if (btnOpenCloseSidebar === "btn-close-sidebar")
    btnSidebarEl.classList.replace("btn-close-sidebar", "btn-open-sidebar");
  if (btnOpenCloseSidebar === "btn-open-sidebar")
    btnSidebarEl.classList.replace("btn-open-sidebar", "btn-close-sidebar");
});
