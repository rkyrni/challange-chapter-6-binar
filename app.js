const express = require("express");
const expressLayouts = require("express-ejs-layouts");
const methodOverride = require("method-override");
const session = require("express-session");
const cookieParser = require("cookie-parser");
const flash = require("connect-flash");
const bodyParser = require("body-parser");
const authenticationModule = require("./module/authentication");
const pagesModule = require("./module/pages");

const app = express();
const port = 3000;

app.set("view engine", "ejs");
app.use(expressLayouts);
app.use(express.static("public"));
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));
app.use(methodOverride("_method"));
app.use(cookieParser("secret"));
app.use(
  session({
    cookie: { maxAge: 6000 },
    secret: "secret",
    resave: true,
    saveUninitialized: true,
  })
);
app.use(flash());

app.get("/", pagesModule.home);

app.get("/list", pagesModule.list);

app.post("/list", pagesModule.search);

app.get("/list/:name", pagesModule.user);

app.get("/detail/:name", pagesModule.detail);

app.get("/services", pagesModule.services);

app.get("/about", pagesModule.about);

app.get("/form", pagesModule.form);

app.post("/form", pagesModule.create);

app.get("/form/:id", pagesModule.edit);

app.put("/list", pagesModule.update);

app.delete("/list", pagesModule.delete);

app.get("/login", pagesModule.login);

app.post("/login", authenticationModule.login);

app.use(pagesModule.notFound);

app.listen(port, () => {
  console.log(`Reading http://localhost:${port}`);
});
