const {
  user_game,
  user_game_biodata,
  user_game_history,
} = require("../../models");

const dataListSidebar = [
  {
    label: "Home",
    link: "/",
  },
  {
    label: "List Users",
    link: "/list",
  },
  {
    label: "Services",
    link: "/services",
  },
  {
    label: "About",
    link: "/about",
  },
];

let counterAuth = false;
const setCounterAuth = () => {
  counterAuth = !counterAuth;
};

exports.home = (req, res) => {
  if (!counterAuth) res.redirect("/login");
  else
    res.render("pages/home", {
      tittle: "Home",
      layout: "layouts/dashboard",
      dataListSidebar,
      usersFilter: [],
    });
};

exports.list = async (req, res) => {
  if (!counterAuth) res.redirect("/login");
  else {
    const users = await user_game_biodata.findAll({
      include: user_game,
    });
    res.render("pages/listUsers", {
      tittle: "List Users",
      layout: "layouts/dashboard",
      dataListSidebar,
      users,
      usersFilter: [],
      msg: req.flash("flash_msg"),
    });
  }
};

exports.services = (req, res) => {
  if (!counterAuth) res.redirect("/login");
  else
    res.render("pages/services", {
      tittle: "Services",
      layout: "layouts/dashboard",
      dataListSidebar,
      usersFilter: [],
    });
};

exports.about = (req, res) => {
  if (!counterAuth) res.redirect("/login");
  else
    res.render("pages/about", {
      tittle: "About",
      layout: "layouts/dashboard",
      dataListSidebar,
      usersFilter: [],
    });
};

exports.form = async (req, res) => {
  if (!counterAuth) res.redirect("/login");
  else {
    res.render("pages/form", {
      tittle: "Form",
      layout: "layouts/general",
    });
  }
};

exports.create = async (req, res) => {
  if (!counterAuth) res.redirect("/login");
  else {
    try {
      const user = await user_game.create(req.body);

      const data = {
        user_id: user.id,
        ...req.body,
      };
      console.log(data + "-----------");
      await user_game_biodata.create(data);
      await user_game_history.create(data);
      req.flash("flash_msg", "User berhasil ditambahkan!");
      res.redirect("/list");
    } catch (err) {
      console.log(err);
    }
  }
};

exports.search = async (req, res) => {
  if (!counterAuth) res.redirect("/login");
  else {
    const searchInput = req.body.search;
    const users = await user_game_biodata.findAll({
      include: user_game,
    });
    const usersFilter = users.filter((user) => {
      return user.name.toLowerCase().includes(searchInput.toLowerCase());
    });
    res.render("pages/listUsers", {
      tittle: "List Users",
      layout: "layouts/dashboard",
      dataListSidebar,
      users,
      msg: [],
      usersFilter,
    });
  }
};

exports.user = async (req, res) => {
  if (!counterAuth) res.redirect("/login");
  else {
    const users = await user_game_biodata.findAll({
      where: { name: req.params.name },
      include: user_game,
    });
    res.render("pages/listUsers", {
      tittle: "list",
      layout: "layouts/dashboard",
      dataListSidebar,
      users,
      msg: [],
      usersFilter: [],
    });
  }
};

exports.detail = async (req, res) => {
  if (!counterAuth) res.redirect("/login");
  else {
    const allUsers = await user_game_biodata.findAll();

    const users = await user_game_biodata.findAll({
      where: { name: req.params.name },
      include: user_game,
    });

    console.log(users[0]);
    const userHistory = await user_game_history.findOne({
      where: {
        user_id: users[0].user_id,
      },
    });

    res.render("pages/detail", {
      tittle: "detail",
      layout: "layouts/dashboard",
      dataListSidebar,
      users,
      allUsers,
      userHistory,
      usersFilter: [],
    });
  }
};

exports.edit = async (req, res) => {
  if (!counterAuth) res.redirect("/login");
  else {
    const userId = req.params.id;
    const user = await user_game_biodata.findOne({
      where: { id: userId },
      include: user_game,
    });
    res.render("pages/formEdit", {
      tittle: "Form",
      layout: "layouts/general",
      user,
    });
  }
};

exports.update = async (req, res) => {
  if (!counterAuth) res.redirect("/login");
  else {
    const newUser = req.body;
    const user = await user_game.findOne({
      where: { id: req.body.id },
    });
    const userBiodata = await user_game_biodata.findOne({
      where: { id: user.id },
    });

    newUser.updatedAt = new Date();
    user.update(newUser);
    userBiodata.update(newUser);
    await user.save();
    await userBiodata.save();
    req.flash("flash_msg", "Data user berhasil di edit!");
    res.redirect("/list");
  }
};

exports.delete = async (req, res) => {
  const id = req.body.idUser;
  const user = await user_game_biodata.findOne({
    where: { user_id: id },
  });
  const userHistory = await user_game_history.findOne({
    where: { user_id: id },
  });
  const usergame = await user_game.findOne({
    where: { id: id },
  });

  await user.destroy();
  await userHistory.destroy();
  await usergame.destroy();
  req.flash("flash_msg", "Data user berhasil di hapus!");
  res.redirect("/list");
};

exports.login = (req, res) => {
  counterAuth = false;
  res.render("pages/login", {
    tittle: "login",
    layout: "layouts/general",
    statusLogin: true,
  });
};

exports.notFound = (req, res) => {
  res.status(404);
  res.render("pages/404", { tittle: "404", layout: "layouts/general" });
};

module.exports.setCounterAuth = setCounterAuth;
