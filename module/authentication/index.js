const dataUsers = require("../../data/users.json");
const { setCounterAuth } = require("../pages");

exports.login = (req, res) => {
  const userLogin = req.body;

  const user = dataUsers.find(
    (data) =>
      data.email.toLowerCase() === userLogin.email.toLowerCase() &&
      data.password === userLogin.password
  );
  if (!user) {
    console.log("gagal");
    res.status(404);
    res.render("pages/login", {
      tittle: "login",
      layout: "layouts/general",
      statusLogin: false,
    });
  } else {
    setCounterAuth();
    res.redirect(`/`);
  }
};
